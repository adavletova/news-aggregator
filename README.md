Сервис загружает новости из новостных сайтов и выводит их список. 
Для запуска сервиса необходимо склонировать проект, перейти в папку с проектом и выполнить команду

`
docker-compose up
`

Вэб-интерфейс станет доступен по адресу http://localhost:4200.

Примеры ресурсов, которые можно подключить к аггрегатору, описаны в файле sources.json

Сервис состоит из трех микросервисов: база данных Postgresql, server, web. 
В данном репозитории расположены исходники server, исходники web - https://gitlab.com/adavletova/news-aggregator-web

Для сборки Docker образ server используется сборочная система Bazel. Чтобы упростить использование Bazel, используется обёртка Bazelisk. Для локальной сборки образа необходимо установить [Bazelisk](https://github.com/bazelbuild/bazelisk)

Для сборки образа выполните следующую команду

`
bazelisk run //:image
`

Если собираете на MacOS:

`
bazelisk run --platforms=@io_bazel_rules_go//go/toolchain:linux_amd64 //:image
`

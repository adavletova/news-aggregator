module adavletova/news-aggregator

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/antchfx/xmlquery v1.2.4
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/gofrs/uuid v3.3.0+incompatible // indirect
	github.com/golang/protobuf v1.4.1
	github.com/grpc-ecosystem/grpc-gateway v1.9.0
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/lib/pq v1.8.0
	github.com/pkg/errors v0.8.1
	github.com/pressly/goose v2.6.0+incompatible
	github.com/shopspring/decimal v1.2.0 // indirect
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.7.1
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013
	google.golang.org/grpc v1.27.0
	google.golang.org/protobuf v1.25.0
)

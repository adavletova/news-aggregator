package main

import (
	"context"
	"database/sql"

	"adavletova/news-aggregator/migration"
	"adavletova/news-aggregator/pkg/config"
	"adavletova/news-aggregator/pkg/server"
	"adavletova/news-aggregator/pkg/store"

	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
)

func main() {
	conf, err := config.ReadConfig("", "")
	if err != nil {
		logrus.WithError(err).Fatal("Failed to read config")
	}
	if conf.Debug {
		logrus.SetLevel(logrus.DebugLevel)
	}

	db, err := sql.Open(conf.DB.Adapter, conf.DB.ToDSN())
	if err != nil {
		logrus.WithError(err).Fatal("failed connect to database")
	}
	defer db.Close()

	ctx := context.Background()

	migr := migration.NewMigration(db)
	if err := migr.Run(ctx); err != nil {
		logrus.WithError(err).Fatal("Failed run migrations")
	}

	st := store.NewStore(conf.DB)
	if err != nil {
		logrus.WithError(err).Fatal("Create store failed")
	}

	srv := server.Server{
		Store: st,
		Conf:  conf.Server,
	}

	if err := srv.Run(ctx); err != nil {
		logrus.WithError(err).Fatal("Start server failed")
	}
}

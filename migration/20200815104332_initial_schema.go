package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20200815104332, Down20200815104332)
}

func Up20200815104332(tx *sql.Tx) error {
	_, err := tx.Exec(`
CREATE TYPE source_type AS ENUM ('HTML', 'XML');

CREATE TABLE source (
	id 				SERIAL PRIMARY KEY,
	type 			source_type NOT NULL,
	url		 		TEXT UNIQUE NOT NULL,
	block			TEXT NOT NULL,
	title			TEXT NOT NULL,
	link 			TEXT NOT NULL,
	root_url		TEXT
);

CREATE TABLE news_item (
	id 				BIGSERIAL PRIMARY KEY,
	title 			TEXT NOT NULL,
	link		 	TEXT UNIQUE NOT NULL,
	create_time		TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	source			TEXT
);
`)
	return err
}

func Down20200815104332(tx *sql.Tx) error {
	_, err := tx.Exec(`
DROP TABLE news;
DROP TABLE source;
`)
	return err
}

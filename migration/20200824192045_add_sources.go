package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20200824192045, Down20200824192045)
}
func Up20200824192045(tx *sql.Tx) error {
	_, err := tx.Exec(`
INSERT INTO source(url, type, block, title, link, root_url) 
VALUES 
('https://lenta.ru/parts/news/','HTML','#more > div','div.titles > h3 > a','div.titles > h3 > a','https://lenta.ru'),
('https://meduza.io/rss/all','XML','//item', '//title','//link', 'https://meduza.io')
`)
	return err
}

func Down20200824192045(tx *sql.Tx) error {
	_, err := tx.Exec(`
DELETE FROM source 
WHERE url = 'https://lenta.ru/parts/news/' OR url = 'https://meduza.io/rss/all';
`)
	return err
}


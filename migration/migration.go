package migration

import (
	"context"
	"database/sql"
	"fmt"
	"sync"
	"time"

	"github.com/pressly/goose"
	"github.com/sirupsen/logrus"
)

const (
	naSchemeName     = "news"
	naGooseTableName = "news_migrations"
)

var (
	// Мьютекс синхронизирующий запуск миграций
	gooseMu = &sync.Mutex{}
)

type Migration struct {
	db         *sql.DB
	schemaName string
}

func NewMigration(db *sql.DB) *Migration {
	migration := &Migration{
		db: db,
	}
	return migration
}

func (m *Migration) Run(ctx context.Context) error {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	if err := m.runGooseMigration(); err != nil {
		return fmt.Errorf("failed to run migration with error: %v", err)
	}

	return nil
}

func (m *Migration) runGooseMigration() error {
	gooseMu.Lock()
	defer gooseMu.Unlock()

	goose.SetTableName(naGooseTableName)
	goose.SetVerbose(false)
	goose.SetLogger(logrus.StandardLogger())

	if err := goose.Up(m.db, "."); err != nil {
		return fmt.Errorf("goose.Up failed with error: %v", err)
	}

	return nil
}

package config

import (
	"fmt"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type Config struct {
	Debug           bool
	Server          *Server
	DB              *DB
	NewsTaskService *NewsTaskService
}

type DB struct {
	Adapter  string
	Host     string
	Port     string
	User     string
	Name     string
	Password string
	Schema   string
}

type NewsTaskService struct {
	LoadNewsInterval time.Duration
}

type Server struct {
	ListenAddr       string
	LoadNewsInterval time.Duration
	SourceFilePath   string
	GrpcListenAddr   string
}

func ReadConfig(path string, filename string) (*Config, error) {
	v := viper.New()
	v.AddConfigPath(path)
	v.SetConfigName(filename)

	v.AutomaticEnv()
	v.SetEnvPrefix("na")
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if err := v.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigParseError); ok {
			return nil, errors.Wrap(err, "parse config file failed")
		}
		logrus.Info("No configuration file loaded")
	}

	v.SetDefault("debug", true)
	v.SetDefault("server.listenaddr", ":8080")
	v.SetDefault("db.host", "127.0.0.1")
	v.SetDefault("db.adapter", "postgres")
	v.SetDefault("db.port", "5432")
	v.SetDefault("db.user", "news")
	v.SetDefault("db.password", "")
	v.SetDefault("db.name", "news")
	v.SetDefault("db.schema", "news")
	v.SetDefault("sourcereader.filepath", "sources.json")
	v.SetDefault("newstaskservice.loadnewsinterval", "1h")
	v.SetDefault("server.loadnewsinterval", "1h")
	v.SetDefault("server.sourcefilepath", "sources.json")
	v.SetDefault("server.grpclistenaddr", ":9090")

	var c Config
	if err := v.Unmarshal(&c); err != nil {
		return nil, errors.Wrap(err, "unable to decode config to struct")
	}

	return &c, nil
}

func (c *DB) ToDSN() string {
	schema := "public"
	if c.Schema != "" {
		schema = c.Schema
	}
	return fmt.Sprintf("host=%s port=%s dbname=%s user=%s sslmode=disable search_path=%s",
		c.Host, c.Port, c.Name, c.User, schema)
}

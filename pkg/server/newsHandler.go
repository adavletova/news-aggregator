package server

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	pb "adavletova/news-aggregator/api/news/v1beta"
	"github.com/PuerkitoBio/goquery"
	"github.com/antchfx/xmlquery"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"adavletova/news-aggregator/pkg/store"
)

func (s *Server) ListNews(ctx context.Context, req *pb.ListNewsRequest) (*pb.ListNewsResponse, error) {
	newsItems, nextPageToken, err := s.Store.ListNewsItems(ctx, req.PageToken, req.PageSize)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "list news items failed")
	}

	var newsItemsProto []*pb.NewsItem
	for _, n := range newsItems {
		newsItemsProto = append(newsItemsProto, newsToProto(n))
	}

	return &pb.ListNewsResponse{
		NewsItems:     newsItemsProto,
		NextPageToken: nextPageToken,
	}, nil
}

func (s *Server) SearchNews(ctx context.Context, req *pb.SearchNewsRequest) (*pb.SearchNewsResponse, error) {
	newsItems, nextPageToken, err := s.Store.SearchNewsItems(ctx, req.Filter, req.PageToken, req.PageSize)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Search news items failed")
	}

	var newsItemsProto []*pb.NewsItem
	for _, n := range newsItems {
		newsItemsProto = append(newsItemsProto, newsToProto(n))
	}

	return &pb.SearchNewsResponse{
		NewsItems:     newsItemsProto,
		NextPageToken: nextPageToken,
	}, nil
}


func newsToProto(news *store.NewsItem) *pb.NewsItem {
	return &pb.NewsItem{
		Name:       "/news/" + strconv.Itoa(int(news.ID)),
		Title:      news.Title,
		Link:       news.Link,
		CreateTime: MustTimestampProto(&news.CreateTime),
		Source: news.Source,
	}
}

func (s *Server) loadNews(ctx context.Context) {
	sources, err := s.Store.ListNewsSource(ctx)
	if err != nil {
		logrus.WithError(err).Error("list sources failed")
		return
	}

	var newsItems []*store.NewsItem
	for _, s := range sources {
		oneSourceNewsItems, err := loadNewsFromSource(s)
		if err != nil {
			logrus.WithError(err).Errorf("load news items from %s failed", s.URL)
			continue
		}

		newsItems = append(newsItems, oneSourceNewsItems...)
	}

	for _, newsItem := range newsItems {
		if err := s.Store.AddNewsItem(ctx, newsItem); err != nil {
			logrus.WithError(err).Errorf("write news to database failed")
		}
	}
}

func loadNewsFromSource(ns *store.NewsSource) ([]*store.NewsItem, error) {
	if ns.Type == store.HTML {
		return loadNewsFromHTML(ns)
	}
	return loadNewsFromXML(ns)
}

func loadNewsFromHTML(ns *store.NewsSource) ([]*store.NewsItem, error) {
	res, err := http.Get(ns.URL)
	if err != nil {
		return nil, errors.Wrapf(err, "get newsItems from %s failed", ns.URL)
	}

	if res.StatusCode != 200 {
		return nil, fmt.Errorf("request by URL %s returned status code %v", ns.URL, res.StatusCode)
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return nil, errors.Wrap(err, "load html from an io.Reader failed")
	}

	var newsItems []*store.NewsItem
	doc.Find(ns.BlockPath).Each(func(i int, s *goquery.Selection) {
		link := s.Find(ns.LinkPath).AttrOr("href", "none")
		if link != "none" {
			item := store.NewsItem{
				Title:  s.Find(ns.TitlePath).Text(),
				Link:   getSourceURL(ns.URL) + strings.TrimSuffix(link, getSourceURL(ns.URL)),
				Source: ns.RootURL,
			}

			newsItems = append(newsItems, &item)
		}
	})

	if len(newsItems) == 0 {
		logrus.Errorf("load news items from %v return empty list", ns.URL)
	}

	return newsItems, nil
}

func loadNewsFromXML(ns *store.NewsSource) ([]*store.NewsItem, error) {
	resp, err := http.Get(ns.URL)
	if err != nil {
		return nil, errors.Wrapf(err, "get newsItems from %s failed", ns.URL)
	}

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("request by URL %s returned status code %v", ns.URL, resp.StatusCode)
	}

	doc, err := xmlquery.Parse(resp.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "parse response body from %s failed", ns.URL)
	}

	nodes, err := xmlquery.QueryAll(doc, ns.BlockPath)
	if err != nil {
		return nil, errors.Wrapf(err, "search news block (by path %s) from %s failed", ns.BlockPath, ns.URL)
	}
	if len(nodes) == 0 {
		return nil, fmt.Errorf("list of newsItems from %s (path to news block:  %s) is empty", ns.URL, ns.BlockPath)
	}

	var newsItems []*store.NewsItem
	for _, n := range nodes {
		title, err := xmlquery.Query(n, ns.TitlePath)
		if err != nil {
			logrus.WithError(err).Errorf("search title by path: %s failed.\nNews source: %s", ns.TitlePath, ns.URL)
			continue
		}

		link, err := xmlquery.Query(n, ns.LinkPath)
		if err != nil {
			logrus.WithError(err).Errorf("search link by path: %s failed.\nNews source: %s", ns.TitlePath, ns.URL)
			continue
		}

		item := store.NewsItem{
			Title:  title.InnerText(),
			Link:   link.InnerText(),
			Source: ns.RootURL,
		}
		newsItems = append(newsItems, &item)
	}

	if len(newsItems) == 0 {
		logrus.Errorf("load news items from %v return empty list", ns.URL)
	}

	return newsItems, nil
}

func getSourceURL(url string) string {
	domain := strings.TrimPrefix(url, "https://")
	i := strings.Index(domain, "/")
	if i == -1 {
		return url
	}

	return "https://" + domain[0:i]
}

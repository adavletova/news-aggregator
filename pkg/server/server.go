package server

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"time"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"

	pb "adavletova/news-aggregator/api/news/v1beta"

	"adavletova/news-aggregator/pkg/config"
	"adavletova/news-aggregator/pkg/store"
)

type Server struct {
	Store *store.Store
	Conf  *config.Server
}

func (s *Server) Run(ctx context.Context) error {
	go s.loadNewsOnShedule(ctx)

	ln, err := net.Listen("tcp", s.Conf.GrpcListenAddr)
	if err != nil {
		return fmt.Errorf("listen grpc addr: %s failed with error: %v", s.Conf.GrpcListenAddr, err)
	}

	g, gctx := errgroup.WithContext(ctx)

	g.Go(func() error {
		gwMux := runtime.NewServeMux(
			runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{Indent: "\t", OrigName: false}),
		)

		conn, err := grpc.DialContext(ctx, s.Conf.GrpcListenAddr, grpc.WithInsecure())
		if err != nil {
			return fmt.Errorf("create grpc.Connection to %s failed with error: %v", s.Conf.GrpcListenAddr, err)
		}
		defer conn.Close()

		if err := pb.RegisterNewsAggregatorHandler(gctx, gwMux, conn); err != nil {
			return fmt.Errorf("RegisterNewsAggregatorHandler failed with error: %v", err)
		}

		httpSrv := &http.Server{
			Addr:    s.Conf.ListenAddr,
			Handler: gwMux,
		}

		go func() {
			<-gctx.Done()
			httpSrv.Shutdown(context.Background())
		}()

		return httpSrv.ListenAndServe()
	})

	g.Go(func() error {
		grpcSrv := grpc.NewServer()

		pb.RegisterNewsAggregatorServer(grpcSrv, s)

		go func() {
			<-gctx.Done()
			grpcSrv.GracefulStop()
		}()

		return grpcSrv.Serve(ln)
	})

	return g.Wait()
}

func (s *Server) loadNewsOnShedule(ctx context.Context) {
	s.loadNews(ctx)

	ticker := time.NewTicker(s.Conf.LoadNewsInterval)
	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			s.loadNews(ctx)
		}
	}
}

package server

import (
	"context"
	"strconv"
	"strings"

	pb "adavletova/news-aggregator/api/news/v1beta"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"adavletova/news-aggregator/pkg/store"
)


func (s *Server) CreateSource(ctx context.Context, req *pb.CreateSourceRequest) (*pb.Source, error) {
	source := &store.NewsSource{
		Type:      typeProtoToStore(req.Source.Type),
		URL:       req.Source.Url,
		BlockPath: req.Source.BlockPath,
		TitlePath: req.Source.TitlePath,
		LinkPath:  req.Source.LinkPath,
		RootURL:   req.Source.RootUrl,
	}

	newsItems, err := loadNewsFromSource(fixSource(source))
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "load news items failed")
	}

	resultSource, err := s.Store.AddNewsSource(ctx, source)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "add news source failed wih error: %v", err)
	}

	for _, newsItem := range newsItems {
		if newsItem.Link == "" || newsItem.Title == "" {
			continue
		}
		if err := s.Store.AddNewsItem(ctx, newsItem); err != nil {
			logrus.WithError(err).Error("add news item failed")
		}
	}

	return sourceToProto(resultSource), nil
}

func sourceToProto(source *store.NewsSource) *pb.Source {
	return &pb.Source{
		Name: "/sources/" + strconv.Itoa(int(source.ID)),
		Url: source.URL,
		Type: typeSourceToProto(source.Type),
		BlockPath: source.BlockPath,
		TitlePath: source.TitlePath,
		LinkPath: source.LinkPath,
		RootUrl: source.RootURL,
	}
}

func fixSource(s *store.NewsSource) *store.NewsSource {
	s.URL = strings.TrimSpace(s.URL)
	s.TitlePath = strings.TrimSpace(s.TitlePath)
	s.LinkPath = strings.TrimSpace(s.LinkPath)
	s.RootURL = strings.TrimSpace(s.RootURL)

	if s.Type == store.HTML {
		j := strings.LastIndex(s.BlockPath, ":nth-child(")
		if j != -1 {
			s.BlockPath = s.BlockPath[:j]
		}
	}

	return s
}


func typeProtoToStore(t pb.Type) store.Type {
	if t.String() == string(store.HTML) {
		return store.HTML
	}
	return store.XML
}

func typeSourceToProto(t store.Type) pb.Type {
	if t == "HTML" {
		return pb.Type_HTML
	}
	return pb.Type_XML
}



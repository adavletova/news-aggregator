package server

import (
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/lib/pq"
)

// MustTimestampProto преобразует time.Time в его protobuf аналог.
// Кидает панику в случае некорректного времени.
func MustTimestampProto(timePointer *time.Time) *timestamp.Timestamp {
	var t time.Time

	if timePointer == nil {
		t = time.Time{}
	} else {
		t = *timePointer
	}

	timestamp, err := ptypes.TimestampProto(t)
	if err != nil {
		panic(err)
	}
	return timestamp
}

func MustTimestamp(tspb *timestamp.Timestamp) time.Time {
	t, err := ptypes.Timestamp(tspb)
	if err != nil {
		panic(err)
	}
	return t
}

func MustNullTimestampProto(time pq.NullTime) *timestamp.Timestamp {
	if !time.Valid {
		t, err := ptypes.TimestampProto(time.Time)
		if err != nil {
			panic(err)
		}
		return t
	}
	t, err := ptypes.TimestampProto(time.Time)
	if err != nil {
		panic(err)
	}
	return t
}

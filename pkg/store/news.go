package store

import (
	"context"
	"math"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const maxPageSize = 100

type NewsItem struct {
	ID         int64
	Title      string
	Link       string
	CreateTime time.Time
	Source 	string
}

type newsPageToken struct {
	ID int64 `json:"ID"`
}

func (s *Store) AddNewsItem(ctx context.Context, item *NewsItem) error {
	sqlStat := `
INSERT INTO news_item(title, link, source)
VALUES ($1, $2, $3)
ON CONFLICT (link)
DO NOTHING
`
	_, err := s.connPool.ExecEx(ctx, sqlStat, nil, item.Title, item.Link, item.Source)
	if err != nil {
		return errors.Wrap(err, "add news item failed")
	}

	return nil
}

func (s *Store) ListNewsItems(ctx context.Context, pageToken string, pageSize int32) ([]*NewsItem, string, error) {
	if pageSize < 1 || pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	var pt = newsPageToken{
		ID: math.MaxInt64,
	}
	if err := Unmarshal(pageToken, &pt); err != nil {
		return nil, "", errors.Wrapf(err, "unmarshal page token")
	}

	sqlStat := `
SELECT id, title, link, create_time, source
FROM news_item 
WHERE id < $1
ORDER BY id DESC
LIMIT $2`
	rows, err := s.connPool.QueryEx(ctx, sqlStat, nil, pt.ID, pageSize)
	if err != nil {
		return nil, "", errors.Wrap(err, "build sql query failed")
	}

	var newsItems []*NewsItem
	for rows.Next() {
		newsItem, err := readNewsItem(rows)
		if err != nil {
			logrus.WithError(err).Errorf("read news item from sql.Row failed")
			continue
		}

		newsItems = append(newsItems, newsItem)
	}

	if len(newsItems) == 0 {
		return newsItems,  "", nil
	}

	var nextPageToken string
	if len(newsItems) == int(pageSize) {
		lastNews := newsItems[len(newsItems)-1]
		nextPageToken, err = Marshal(&newsPageToken{
			ID: lastNews.ID,
		})
		if err != nil {
			return nil, "", errors.Wrap(err, "marshal page token failed")
		}
	}

	return newsItems, nextPageToken, nil
}

func (s *Store) SearchNewsItems(ctx context.Context, filter, pageToken string, pageSize int32) ([]*NewsItem, string, error) {
	if pageSize < 1 || pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	var pt = newsPageToken{
		ID: math.MaxInt64,
	}
	if err := Unmarshal(pageToken, &pt); err != nil {
		return nil, "", errors.Wrap(err, "unmarshal page token failed")
	}

	sqlStat := `
SELECT id, title, link, create_time, source
FROM news_item 
WHERE id < $1
AND title LIKE $2
ORDER BY id ASC
LIMIT $3`
	rows, err := s.connPool.QueryEx(ctx, sqlStat, nil, pt.ID, "%" + filter + "%" , pageSize)
	if err != nil {
		return nil, "", errors.Wrap(err, "queryEx failed")
	}

	var newsItems []*NewsItem
	for rows.Next() {
		newsItem, err := readNewsItem(rows)
		if err != nil {
			logrus.WithError(err).Errorf("read news item from sql.Row failed")
			continue
		}

		newsItems = append(newsItems, newsItem)
	}

	if len(newsItems) == 0 {
		return newsItems,  "", nil
	}

	var nextPageToken string
	if len(newsItems) == int(pageSize) {
		lastNews := newsItems[len(newsItems)-1]
		nextPageToken, err = Marshal(&newsPageToken{
			ID: lastNews.ID,
		})
		if err != nil {
			return nil, "", errors.Wrap(err, "marshal page token failed")
		}
	}

	return newsItems, nextPageToken, nil
}

func readNewsItem(scanner rowScanner) (*NewsItem, error) {
	var n NewsItem
	if err := scanner.Scan(&n.ID, &n.Title, &n.Link, &n.CreateTime, &n.Source); err != nil {
		return nil, errors.Wrap(err, "scan news from sql row failed")
	}
	return &n, nil
}

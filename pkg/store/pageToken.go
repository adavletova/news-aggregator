package store

import (
	"encoding/base64"
	"encoding/json"
)

// Marshal возвращает строковое представление токена.
// В виде строкового представления используется base64-закодированный JSON токена.
func Marshal(v interface{}) (string, error) {
	b, err := json.Marshal(v)
	if err != nil {
		return "", err
	}

	return base64.URLEncoding.EncodeToString(b), nil
}

// Unmarshal парсит строку в токен.
func Unmarshal(data string, v interface{}) error {
	if data == "" {
		return nil
	}

	b, err := base64.URLEncoding.DecodeString(data)
	if err != nil {
		return err
	}

	return json.Unmarshal(b, v)
}

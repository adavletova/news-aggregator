package store

import (
	"context"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type NewsSource struct {
	ID        int64
	Type      Type `json:"type"`
	URL       string `json:"url"`
	BlockPath string `json:"blockpath"`
	TitlePath string `json:"titlepath"`
	LinkPath  string `json:"linkpath"`
	RootURL	string `json:"rooturl"`
}

func (s *Store) AddNewsSource(ctx context.Context, ns *NewsSource) (*NewsSource, error) {
	sqlStat := `
INSERT INTO source(url, type, block, title, link, root_url) 
VALUES($1, $2, $3, $4, $5, $6)
ON CONFLICT (url)
DO UPDATE 
SET block = EXCLUDED.block, title = EXCLUDED.title, link = EXCLUDED.link, root_url = EXCLUDED.root_url
RETURNING id, url, type, block, title, link, root_url;
`
	rows := s.connPool.QueryRowEx(ctx, sqlStat, nil, ns.URL, ns.Type, ns.BlockPath, ns.TitlePath, ns.LinkPath, ns.RootURL)

	source, err := readNewsSource(rows)
	if err != nil {
		return nil, errors.Wrap(err, "add news source failed")

	}

	return source, nil
}

func (s *Store) ListNewsSource(ctx context.Context) ([]*NewsSource, error) {
	sqlStat := `
SELECT id, url, type, block, title, link, root_url
FROM source 
ORDER BY id ASC
`
	rows, err := s.connPool.QueryEx(ctx, sqlStat, nil)
	if err != nil {
		return nil, errors.Wrap(err, "queryEx failed")
	}

	var newsSources []*NewsSource
	for rows.Next() {
		newsSource, err := readNewsSource(rows)
		if err != nil {
			logrus.WithError(err).Errorf("read news source from sql.Row failed")
			continue
		}

		newsSources = append(newsSources, newsSource)
	}

	return newsSources, nil
}

func readNewsSource(scanner rowScanner) (*NewsSource, error) {
	var s NewsSource
	if err := scanner.Scan(&s.ID, &s.URL, &s.Type, &s.BlockPath, &s.TitlePath, &s.LinkPath, &s.RootURL); err != nil {
		return nil, errors.Wrap(err, "scan source from sql row failed")
	}
	return &s, nil
}

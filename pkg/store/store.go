package store

import (
	"github.com/jackc/pgx"
	"github.com/sirupsen/logrus"

	"adavletova/news-aggregator/pkg/config"
)

type Type string

const (
	HTML Type = "HTML"
	XML  Type = "XML"
)

type Store struct {
	connPool *pgx.ConnPool
}

type rowScanner interface {
	Scan(...interface{}) error
}

func NewStore(conf *config.DB) *Store {
	connConfig, err := pgx.ParseConnectionString(conf.ToDSN())
	if err != nil {
		logrus.WithError(err).Fatal("parse connection string failed")
	}

	pool, err := pgx.NewConnPool(pgx.ConnPoolConfig{ConnConfig: connConfig})
	if err != nil {
		logrus.WithError(err).Fatal("create connection pool failed")
	}

	return &Store{connPool: pool}
}
